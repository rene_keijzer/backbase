'use strict';

/**
 * @ngdoc function
 * @name formula1App.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the formula1App
 */
angular.module('formula1App')
  .controller('MainCtrl', ['$scope', '$http', function ($scope, $http) {

    //http requests
    this.getStandings = function(year, succesCallback, failedCallback){
      $http({
        method: 'GET',
        url: 'http://ergast.com/api/f1/'+ year +'/driverstandings/1.json'
      }).then(succesCallback, failedCallback);

    }
    $scope.getRaceData = function(year, succesCallback, failedCallback){
      $http({
        method: 'GET',
        url: 'http://ergast.com/api/f1/'+year+'/results/1.json'
      }).then(succesCallback, failedCallback);
    }


    $scope.raceDataonClick = function(year){
      $scope.getRaceData(i,
        function(result){
          if(result.status == 200){
            $scope.selectedYear = result.data.MRData.RaceTable.Races;
            $scope.season = year;
            $scope.seasonWinner = $scope.winners[year];
          }
        },
        function(result){
          throw ex;
        }
      );
    }

    //populate starting table
    var startYear = 2005;
    var endYear = 2015;
    $scope.winners= {};
    for(var i = startYear; i < endYear; i++){
        this.getStandings(i, function(result){
          if(result.status == 200){
              $scope.winners[result.data.MRData.StandingsTable.season] = result.data.MRData.StandingsTable.StandingsLists[0].DriverStandings[0].Driver;
        }
      },
      function(result){
        throw ex;
      });
    }










  }]);
