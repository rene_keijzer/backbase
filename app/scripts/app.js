'use strict';

/**
 * @ngdoc overview
 * @name formula1App
 * @description
 * # formula1App
 *
 * Main module of the application.
 */
angular
  .module('formula1App', [
    'ngRoute'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
