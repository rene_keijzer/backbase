# Formula1

This project has been build with the following packages: "npm", "bower", "grunt" and "yeoman"
The application uses twitter bootstrap as css framework html5boilerplate as front-end template


before trying to run the application make sure the following dependencies are installed:
>bower
>grunt
>npm

if not:

Download node here: https://nodejs.org/en/
Then install from the node packet manager:
>npm install -g grunt-cli
>npm install -g bower

If you have the needed packages continue on to build & development

## Build & development

Enter the root directory with a shell and execute the following commands:

>npm install
>bower install

Then Run `grunt` for building and `grunt serve` for preview.
You can preview the page on http://localhost:9000
